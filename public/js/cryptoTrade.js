var br = document.querySelectorAll(".buysRow");
var sl = document.querySelectorAll(".sellsRow");
var sellRate = document.querySelector(".sellRate");
var sellAmount = document.querySelector(".sellAmount");
var sellTotal = document.querySelector(".sellTotal");
var buyRate = document.querySelector(".buyRate");
var buyAmount = document.querySelector(".buyAmount");
var buyTotal = document.querySelector(".buyTotal");


//We check first if the fields contain  values 


if(sellRate.value !="" && sellAmount.value !=""){
    sellTotal.value = (sellRate.value * sellAmount.value);
}
if( buyAmount.value !="" && buyRate.value !=""){
    buyTotal.value = (buyRate.value * buyAmount.value); 
}

// Loop for sell rows
for(var i= 0; i<sl.length; i++){

    sl[i].addEventListener('click',function(e){
        var childs = e.target.parentNode.children;
        buyAmount.value="";
        buyTotal.value ="";
        buyRate.value =  parseFloat( childs[1].getAttribute('data-content')) ;
        buyAmount.value = parseFloat(childs[0].getAttribute('data-content'));
        buyTotal.value = parseFloat(buyRate.value * buyAmount.value).toFixed(5);
        
    },false);
}

//Loop for buy rows
for( var i=0;i< br.length; i++){
    br[i].addEventListener("click",function(e){

        
        var childsSl  = e.target.parentNode.children;
        sellAmount.value="";
        sellTotal.value ="";

        sellRate.value= parseFloat( childsSl[1].getAttribute('data-content') ); 
        sellAmount.value = parseFloat(childsSl[0].getAttribute('data-content'));
        sellTotal.value = parseFloat(sellAmount.value * sellRate.value).toFixed(5);



    },false);
}

// Calculate values 
sellRate.addEventListener('keyup',function(e){
    if( sellAmount.value != ""){
        sellTotal.value = parseFloat(sellRate.value * sellAmount.value).toFixed(5);
    }
},false);

sellAmount.addEventListener('keyup',(e)=>{
    if( sellRate.value!=""){
        sellTotal.value = parseFloat(sellRate.value * sellAmount.value).toFixed(5);
    }
},false);

sellTotal.addEventListener('keyup',function(e){
    if(sellAmount.value =="" && sellRate.value !=""){
        sellAmount.value =  (sellTotal.value/ sellRate.value).toFixed(5); 
    }
    if( sellAmount.value !="" && sellRate.value == ""){
        sellRate.value = (sellTotal.value/ sellAmount.value).toFixed(5);
    }
    if( sellAmount.value !="" && sellRate.value != ""){
        sellAmount.value = (sellTotal.value/ sellRate.value).toFixed(5);
    }

},false);

buyRate.addEventListener('keyup',function(e){
    if( buyAmount.value != ""){
        buyTotal.value = parseFloat(buyRate.value * buyAmount.value).toFixed(5);
    }
},false);

buyAmount.addEventListener('keyup',(e)=>{
    if( buyRate.value!=""){
        buyTotal.value = parseFloat(buyRate.value * buyAmount.value).toFixed(5);
    }
},false);

buyTotal.addEventListener('keyup',function(e){
    if(buyAmount.value =="" && buyRate.value !=""){
        buyAmount.value =  (buyTotal.value/ buyRate.value).toFixed(5); 
    }
    if( buyAmount.value !="" && buyRate.value == ""){
        buyRate.value = (buyTotal.value/ buyAmount.value).toFixed(5);
    }
    if( buyAmount.value !="" && buyRate.value != ""){
        buyAmount.value = (buyTotal.value/ buyRate.value).toFixed(5);
    }

},false);