<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// $this->call(UsersTableSeeder::class);
		
		DB::table('wallets')->insert([
			'uid' => 1,
			'wallet_name' => "monero",
			'ticker_symbol' =>"XMR",
			'balance'=> 0,
			'status' => 1
		]);
		DB::table('wallets')->insert([
			'uid' => 1,
			'wallet_name' => "bitcoin",
			'ticker_symbol' =>"BTC",
			'balance'=> 0,
			'status' => 1
		]);
    	$now = Carbon::now();
        for ($i=0; $i <3 ; $i++) { 
        	switch ($i) {
        		case 0:
        			DB::table('pairs')->insert([
        				'base_currency' => 'XMR',
        				'second_currency' => 'BTC',
        				'market_price' => 0.00243,
        				'24_hour_last_price' => 0.4653,
        				'15_min_last_price' => 0.00334,
        				'last_trade' => $now,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			DB::table('buy_orders')->insert([
        				'buyer_id' => 1,
        				'amount' => 0.00324,
        				'price' => 0.00243,
						'status' => 0,
						'total' =>  0.00324 * 0.00243,
						'matched_amount' => 0,
        				'pair_id' => 1,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			DB::table('sell_orders')->insert([
        				'seller_id' => 1,
        				'price' => 0.00221,
        				'amount' => 0.00324,
						'status' => 0,
						'total' =>  0.00221 *  0.00324,
						'matched_amount' => 0,
        				'pair_id' => 1,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			break;
        		case 1:
        			DB::table('pairs')->insert([
        				'base_currency' => 'ETH',
        				'second_currency' => 'DTC',
        				'market_price' => 0.01322,
        				'24_hour_last_price' => 0.0044,
        				'15_min_last_price' => 0.01222,
        				'last_trade' => $now,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			DB::table('buy_orders')->insert([
        				'buyer_id' => 1,
        				'amount' => 0.0543,
        				'price' => 0.0324,
						'status' => 0,
						'total' => 0.0543 * 0.0324,
						'pair_id' => 2,
						'matched_amount' => 0,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			DB::table('sell_orders')->insert([
        				'seller_id' => 1,
        				'price' => 0.4345,
        				'amount' => 0.05432,
						'status' => 0,
						'total' =>0.4345 *0.05432,
						'pair_id' => 2,
						'matched_amount' => 0,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			break;
        		case 2:
        			DB::table('pairs')->insert([
        				'base_currency' => 'BTC',
        				'second_currency' => 'USD',
        				'market_price' => 10000,
        				'24_hour_last_price' => 0.4002,
        				'15_min_last_price' => 0.00334,
        				'last_trade' => $now,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			DB::table('buy_orders')->insert([
        				'buyer_id' => 1,
        				'amount' => 0.07654,
        				'price' => 0.04674,
						'status' => 0,
						'total' => 0.07654 * 0.04674,
						'pair_id' => 3,
						'matched_amount' => 0,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			DB::table('sell_orders')->insert([
        				'seller_id' => 1,
        				'price' => 0.03214,
						'amount' => 0.04567,
						'total' => 0.03214 * 0.04567,
						'matched_amount' => 0,
        				'status' => 0,
        				'pair_id' => 3,
        				'created_at' => $now,
        				'updated_at' => $now
        			]);
        			break;
   
        	}
        }
    }
}
