<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('seller_id')->unsigned();
            
            $table->decimal('price',19,5); // The price at which the trader want to sell one asset
            $table->decimal('amount',19,5);// The amount to sell 
            $table->decimal('matched_amount',19,5); // amount = matched_amount => trade completed 

            $table->foreign('seller_id')  // The id of the user who's placing the sell order
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            
            $table->smallInteger('status'); // @see : buy_orders
            $table->integer('pair_id'); // The ID of the pair
            $table->decimal("total",19,5);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_orders');
    }
}