<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('base_currency',5);
            $table->string('second_currency',5);
            $table->decimal('market_price', 19,5); // market_price == last_price 
            
            $table->decimal('24_hour_last_price', 19,5);
            $table->decimal('15_min_last_price', 19,5);
            $table->timestamp('last_trade'); // The date at which the latest trade was made
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pair');
    }
}
