<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchedTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matched_transactions', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('seller_id')->unsigned();

            $table->integer('buyer_id')->unsigned();

            $table->foreign('seller_id')
                  ->references('id')->on('users')   // The Id of the seller coming from users table
                  ->onDelete('cascade');

            $table->foreign('buyer_id')
                  ->references('id')->on('users')   // The Id of the seller coming from users table
                  ->onDelete('cascade');
            
            $table->decimal('amount',19,5);
            $table->decimal('price',19,5); // The price at

            $table->timestamp('carry_date'); // The date at which the matched was made 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matched_transactions');
    }
}
