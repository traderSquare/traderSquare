<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('last_prices', function (Blueprint $table) {
        //     $table->increments('id');

        //     $table->integer('pair_id'); // TODO : should be set a foreign key of pairs

        //     $table->foreign('pair_id')
        //           ->references('id')->on('pairs')
        //           ->onDelete('cascade');

        //     $table->integer('pair_price'); // The latest price of the pair

        //     $table->dateTime('last_bought'); // The date at which the bought was made

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_prices');
    }
}
