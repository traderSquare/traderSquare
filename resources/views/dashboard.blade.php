@extends('layouts.app')

@section('content')
@section("css")
<style>

a.standard:hover{
    color: #e39706;
    transition: color,1s;
}
thead.thead-inverse{
    background-color: black;
    color: white;
}

.ordersTab .sellsTab{
  height: 230px;
  overflow-y: auto;
  width: 100%;
}
.ordersTab td:hover, .sellsTab td:hover{
    cursor: pointer;
}

.graph1Container {
    width: 980px;
    height:400px;
     margin-bottom:50px;
     border: 1px solid white;

    }


</style>


@endsection

<div class="container graphsContainer">
    <div class="row">
        <div class="col-md-10 graph1Container">
            <!-- TradingView Widget BEGIN -->
            <!-- <script type="text/javascript" src="https://d33t3vvu2t2yu5.cloudfront.net/tv.js"></script> -->
            <script type="text/javascript">
                new TradingView.widget({
                "autosize": true,
                "symbol": "COINBASE:BTCUSD",
                "interval": "D",
                "timezone": "Etc/UTC",
                "theme": "white",
                "style": "1",
                "locale": "en",
                "toolbar_bg": "white",
                "hide_top_toolbar": true,
                "save_image": false,
                "hideideas": true
                });
            </script>
            <!-- TradingView Widget END -->
        </div>
    </div>
</div>

<div class="ordersContainer">
   <div class="row buySellRow">
    <div class="col-md-5"  style="margin-left: 30px">
        <div class="card">
            <div class="card-header" style="background-color:#f3f7f7">
                 @auth
                    <div class="visibleWhenLogin" >
                        <h4>Buy {{$pair->base_currency}} <a href="/deposit/xmr" class="float-right standard" style="font-style:normal;font-size:0.6em; text-decoration:underline;color:black">Deposit xmr</a></h4> 
                        <table class="" border="0" cellspacing="2">
                            <tr>
                                <td><small>You have :</small></td>  
                                <td><span class="base_currency_balance"><small>@if($userBalanceSecondCurrency!=null) {{ $userBalanceSecondCurrency->balance }} @else 0.00000000 @endif {{$pair->second_currency}}</small></span> </td>
                            </tr>
                            <tr>
                                <td><small>Lowest ask :</small></td>
                                <td><span class="second_currency_balance"> <small>0.00023 {{$pair->second_currency}}</small></span></td> 
                            </tr>
                        </table>
                    </div>
                @endauth
                </div>

                <div class="card-body" style="background-color:#dcecec;background-image:linear-gradient(#dcecec, #fff)">
                  <form method="post" action="/buy" autocomplete="off">
                  @csrf
                  @if ( \Session::has('errorMessage') )
                              
                              <div class="alert alert-danger">
                                 {!! \Session::get('errorMessage') !!}
                              </div>
                                     
                         @endif
                  <!-- @include("../notifications") -->
                  @if ( \Session::has('insufficientBalance') )
                        <div class="alert alert-danger">
                             {!! \Session::get('insufficientBalance') !!}
                        </div>
                    @endif
                    <div class="input-group input-group-sm mb-3 col-sm-10 {{ $errors->has('buy_price')? 'has-error':''}} ">
                        <input type="text" name="buy_amount" value="1" class="form-control buyRate" placeholder="Amount" aria-label="base currency value" aria-describedby="basic-addon2">
                        <input type="hidden" name="pairId" value="{{$pair->id}}"/>
                        <div class="input-group-append">
                           <span class="input-group-text" id="basic-addon2">{{$pair->base_currency}}</span>
                        </div>
                        {!! $errors->first('buy_price','<br/><span class="text text-danger help-block">:message</span>')!!}
                    </div>

                    
                    <div class="input-group input-group-sm mb-3 col-sm-10 {{ $errors->has('buy_amount')? 'has-error':''}}">
                        <input type="text" name="buy_price" value="{{$pair->market_price}}" class="form-control buyAmount"  placeholder="price" aria-label="base currency value" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                           <span class="input-group-text" id="basic-addon2">{{$pair->second_currency}}</span>
                        </div><br/>
                        {!! $errors->first('buy_amount','<br/><span class="text text-danger help-block">:message</span>')!!}
                    </div>
                    <hr/>

                   <div class="input-group input-group-sm mb-3 col-sm-10">
                        <input type="text" class="form-control buyTotal"  name="buyTotal" aria-label="base currency value" aria-describedby="basic-addon2" >
                        <div class="input-group-append">
                           <span class="input-group-text" id="basic-addon2">total {{$pair->second_currency}}</span>
                        </div>
                    </div>

                    <br/>
                    <input type="submit" value="buy" class="float-right"/>
                </form>
                    <small> Fee <a href="#priceFee"> 0.15/0.25%</a></small>
                   
                </div>
            </div>
        </div>
        <div class="col-md-5">
        <div class="card">
            <div class="card-header" style="background-color:#f3f7f7">
                 @auth
                    <div class="visibleWhenLogin" >
                        <h4>Sell {{$pair->base_currency}} <a href="/deposit/xmr" class="float-right standard" style="font-style:normal;font-size:0.6em; text-decoration:underline;color:black">Deposit {{$pair->second_currency}}</a></h4> 
                        <table class="" border="0" cellspacing="2">
                            <tr>
                                <td><small>You have :</small></td>  
                                <td><span class="base_currency_balance"><small>@if( $userBalanceBaseCurrency!=null ) {{$userBalanceBaseCurrency->balance }} @else 0.00000000 @endif {{$pair->base_currency}}</small></span> </td>
                            </tr>
                            <tr>
                                <td><small>Highest bid :</small></td>
                                <td><span class="second_currency_balance"> <small> 0.0003883 {{$pair->second_currency}}</small></span></td> 
                            </tr>
                        </table>
                    </div>
                @endauth
                </div>

                <div class="card-body" style="background-color:#dcecec;background-image:linear-gradient(#dcecec, #fff)">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                  <form method="post" action="/sell">
                  @if ( \Session::has('insufficientBal') )
                        <div class="alert alert-danger">
                             {!! \Session::get('insufficientBal') !!}
                        </div>
                    @endif
                  @csrf
                   <!-- @include("../notifications")  -->
                    <div class="input-group input-group-sm mb-3 col-sm-10 {{ $errors->has('price')? 'has-error':''}}">
                        <input type="hidden" name="pairId" value="{{$pair->id}}"/>
                        <input type="text" name="amount" value="1" class="form-control sellRate" placeholder="Amount" aria-label="base currency value" aria-describedby="basic-addon2" >
                        <div class="input-group-append">
                           <span class="input-group-text" id="basic-addon2">{{$pair->base_currency}}</span>
                        </div><br/>
                        {!! $errors->first('price','<br/><span class="text text-danger help-block">:message</span>')!!}
                    </div>

                    
                    <div class="input-group input-group-sm mb-3 col-sm-10 {{ $errors->has('amount')? 'has-error':''}}">
                        <input type="text" name="price" value="{{$pair->market_price}}" class="form-control sellAmount"  placeholder="amount" aria-label="base currency value" aria-describedby="basic-addon2" >
                        <div class="input-group-append">
                           <span class="input-group-text" id="basic-addon2">{{$pair->second_currency}}</span>
                        </div> <br/>
                        {!! $errors->first('amount','<br/><span class="text text-danger help-block">:message</span>')!!}
                    </div>
                    <hr/>
                   <div class="input-group input-group-sm mb-3 col-sm-10 ">
                        <input type="text" class="form-control sellTotal" name="sellTotal" aria-label="base currency value" aria-describedby="basic-addon2" >
                        <div class="input-group-append">
                           <span class="input-group-text" id="basic-addon2">total {{$pair->second_currency}}</span>
                        </div>
                    </div>

                    <br/>
                    <input type="submit" value="Sell" class="float-right"/>
                </form>
                    <small> Fee <a href="#priceFee" class="fees"> 0.15/0.25%</a></small>
                   
                </div>
            </div>
        </div>

    </div>

    <!-- end buySell row --> 
        <br/><br/>
    <div class="row liveUpdateBuySellOrders">

        <div class="col-md-5" style="margin-left: 30px">
             <div class="panel panel-heading">
               <h3>Sell Orders </h3>
                <span class="float-right totalBuysRow"> <small>Total : 1221212 {{$pair->base_currency}}</small> </span>
             </div>
             <table class="table table-striped table-bordered sellsTab">
                <thead class="thead-inverse">
                    <tr class="tickers_descr_sell">
                        <th>Price</th>
                        <th>{{$pair->base_currency}}</th>
                        <th>{{$pair->second_currency}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($all_sells as $sell)
                        <tr class="sellsRow">
                            <td data-content="{{$sell->price}}" class="sellRate">{{$sell->price}}</td>
                            <td data-content="{{$sell->amount}}" class="sellAmount">{{$sell->amount}}</td>
                            <td data-content="{{$sell->total}}" class="sellTotal">{{$sell->total}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            
        </div>

        <div class="col-md-5" style="margin-left: 30px">
             <div class="panel panel-heading">
                <h3>Buy Orders </h3>
                <span class="float-right totalSellsRow"> <small>Total : 1221212 XMR</small> </span>
             </div>
            
             <table class="table table-striped table-bordered ordersTab">
                <thead class="thead-inverse">
                    <tr class="tickers_desc_buy">
                        <th>Price</th>
                        <th>{{$pair->base_currency}}</th>
                        <th>{{$pair->second_currency}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($all_buys as $buy)
                        <tr class="buysRow">
                            <td data-content="{{$buy->price}}" class="orderRate" to="buy">{{$buy->price}}</td>
                            <td data-content="{{$buy->amount}}" class="orderAmount" to="buy">{{$buy->amount}}</td>
                            <td data-content="{{$buy->total}}" class="orderTotal" to="buy">{{$buy->total}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        

    </div>
        
        <!--live update end -->
        



    </div>
</div>

@endsection