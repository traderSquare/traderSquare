<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Redirect;
use DB;
use Carbon\Carbon; 


class dashboardController extends Controller
{

    public function getAllBuysAndSells($pairId=null,$baseCurrency=null,$secondCurrency=null){
		
		if( $pairId !=null && $baseCurrency ==null && $secondCurrency ==null){
			$selectedPair = DB::table('pairs')->whereId((int) $pairId);
			$userBalanceBaseCurrency = DB::table('wallets')->where("ticker_symbol",$selectedPair->base_currency);
			$userBalanceSecondCurrency = DB::table('wallets')->where("ticker_symbol",$selectedPair->second_currency);
		}
		else if ($baseCurrency!=null && $secondCurrency!=null ) { 
			$selectedPair = DB::table('pairs')->where([['base_currency','=',$baseCurrency],['second_currency','=',$secondCurrency]]);
			$userBalanceBaseCurrency = DB::table('wallets')->where("ticker_symbol",$selectedPair->base_currency);
			$userBalanceSecondCurrency = DB::table('wallets')->where("ticker_symbol",$selectedPair->second_currency);
		}else{
			return Redirect::back()->with("errorMessage","Unknown error please try again later");
		}

    	$all_buys = DB::table('buy_orders')->get();
		$all_sells = DB::table('sell_orders')->get(); 
		

    	return view('dashboard', compact('all_buys', 'all_sells','userBalanceBaseCurrency','userBalanceSecondCurrency'));
    }

    public function postBuy(Request $request){
    	$validator = Validator::make($request->all(), [
    		'buy_price' => 'required|regex:/^\d*(\.\d{0,})?$/|min:0.0',
    		'buy_amount' => 'required|regex:/^\d*(\.\d{0,})?$/|min:0.0'
    	]);
	   
		
    
    	if ($validator->fails()) {
    		return Redirect::back()->withInput()->withErrors($validator);
		}
		//Verify user balance on giving pair
		$pairTable = DB::table("pairs")->whereId( (int) $request->pairId)->first();
		$walletOfCurrentUser=DB::table('wallets')->where([ ["uid","=",Auth::id()],["ticker_symbol","=",$pairTable->second_currency] ])->first(); 
	
	
			if($walletOfCurrentUser ==null || $walletOfCurrentUser->balance < $request->buyTotal){
				return Redirect::back()->with("insufficientBalance","You don't have enough ".$pairTable->second_currency);
			}

			$orderId = DB::table('buy_orders')->insertGetId([
    		'price' => $request->buy_price,
            'amount' => $request->buy_amount,
			'buyer_id' => Auth::id(),
			'status' => 0,
			'matched_amount' => 0,
			'total' => $request->buy_price * $request->buy_amount,
			'pair_id' => $request->pairId,
			'created_at' => Carbon::now()
		]);
		

		$this->matchTrade($orderId,'buy_orders');

		return redirect()->route('dashboard');
		
    }


    public function postSell(Request $request){
    	$validator = Validator::make($request->all(), [
    		'price' => 'required|regex:/^\d*(\.\d{0,})?$/',
            'amount' => 'required|regex:/^\d*(\.\d{0,})?$/',
        
        ]);

		if ($validator->fails()) {
    		return Redirect::back()->withInput()->withErrors($validator);
    	}
		//Check if the user has enough base_currency balance 

		$pairTable = DB::table("pairs")->whereId( (int) $request->pairId)->first();
		$walletOfCurrentUser=DB::table('wallets')->where([ ["uid","=",Auth::id()],["ticker_symbol","=",$pairTable->base_currency] ])->first(); 
	
		if( $walletOfCurrentUser == null || $walletOfCurrentUser->balance < $request->amount){
			return Redirect::back()->with("insufficientBal","You don't have enough ".$pairTable->base_currency);
		}

    
    	$orderId = DB::table('sell_orders')->insertGetId([
    		'price' => $request->price,
            'amount' => $request->amount,
			'seller_id' => Auth::id(),
			'status' => 0,
			'matched_amount' => 0,
			'total' => $request->price * $request->amount,
			'pair_id' => $request->pairId, 
			'created_at' => Carbon::now()
			]);
			
		$this->matchTrade($orderId,'sell_orders'); 

    	return redirect()->route('dashboard');
	}
	
public function creditWallet($userId, $tickerSymbol,$amount){

	$walletExists = DB::table("wallets")->where([ ["uid","=",$userId], ["ticker_symbol","=",$tickerSymbol]])->first();
	
	if($walletExists){
		$walletExists = DB::table("wallets")->where([ ["uid","=",$userId], ["ticker_symbol","=",$tickerSymbol]])->increment("balance",$amount);

	}else {
		$insertData = [
					"balance" => $amount,
					"uid" => $userId,
					"ticker_symbol" => $tickerSymbol,
					"created_at" => Carbon::now(),
					"updated_at" => Carbon::now(),
					"status" => 1
					];

	  	DB::table("wallets")->insert($insertData);
		}
		return true;


}

	public function matchTrade($orderId,$tableName){

		$tradeOrder = DB::table($tableName)->whereId($orderId)->first(); 


		$tableName2 = null; 
		switch( $tableName){
			case "sell_orders" :
			   $tableName2 = "buy_orders";
			   $action = "buyer_id";
			   break;
			case "buy_orders":
			   $tableName2 = "sell_orders";
			   $action="seller_id";
			break;
		}

		 //We check if the price matches and pair_id too ... 
		 //For now we're not matching sells/buys made by the current user 

		$findMatch = DB::table($tableName2)->where([["price","=",$tradeOrder->price], ["pair_id","=",$tradeOrder->pair_id]])->orderBy("id","asc")->first();
		if($findMatch) {
			$pair = DB::table("pairs")->whereId($tradeOrder->pair_id)->first(); 

			$requestedAmount = $tradeOrder->amount;
			$matchTradeAmount = $findMatch->amount; 
			
			if( $requestedAmount == $matchTradeAmount) {
				//Full matched => order completed 
				// Update both $tradeOrder and $tableName2 table and set status to completed(3)
				// Credit buyer and debit seller 
				// Insert data inside the matchedTransactions  table 
				// DB::table('matched_transactions')->insert([
				// 	"seller_id" => Auth::id(),
				// 	"buyer_id" => 
				// ]);
				
				if( $findMatch->seller_id == $tradeOrder->buyer_id){
					Redirect::back()->with("errorMessage","Unexpected error occured, please try again later") ;
				}else{
					
					// Inserting inside the matched transactions table
					DB::table('matched_transactions')->insert([
						"seller_id" => $findMatch->seller_id,
						"buyer_id" => $tradeOrder->buyer_id,
						"amount" => $tradeOrder->amount,
						"price" => $tradeOrder->price,
						"carry_date" => Carbon::now(),
						"created_at" => Carbon::now(),
					]);

					$currenctUserWallet = DB::table("wallets")->where("uid",$tradeOrder->buyer_id);
					$bidUserWallet = DB::table("wallets")->where("uid",$findMatch->buyer_id);



					return Rediect::back()->with("success","The transaction was successfully completed");
				}
				
				

			}else if ( $requestedAmount > $matchTradeAmount ){
				//Partially completed for $requestedAmount && fully completed for $matchedTrade 

			}else if( $requestedAmount < $matchTradeAmount ){
				// Partially completed for $matchTradeAmount && fully completed for $requestedTrade 
			}

			// Update pair market price 
		}

	}
}


