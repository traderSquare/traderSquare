<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 
use Redirect; 
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // The default pair selected when the user just arrived in the dashboard
        // TODO : write a function that will return the balance based on uid and ticker_symbol

            $all_buys = DB::table('buy_orders')->get();
            $all_sells = DB::table('sell_orders')->get();
            
            $userBalanceBaseCurrency = DB::table("wallets")->where([ ["uid","=",Auth::id()],["ticker_symbol","=","XMR"] ])->first();
            $userBalanceSecondCurrency = DB::table("wallets")->where([ ["uid","=",Auth::id()] , ["ticker_symbol","=","BTC"]])->first();

            $pair = DB::table('pairs')->where('base_currency', 'XMR')->where('second_currency', 'BTC')->first();

            return view('dashboard', compact('all_buys', 'all_sells', 'pair','userBalanceBaseCurrency','userBalanceSecondCurrency'));
    }
}
