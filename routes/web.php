<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard')->middleware('auth');

Route::post('/dashboard','dashboardController@getAllBuysAndSells')->middleware('auth');


Route::post("/buy","dashboardController@postBuy")->middleware('auth'); 

Route::post("/sell","dashboardController@postSell")->middleware('auth');

